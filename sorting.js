var views_table_sorting_order = 'ASC';

(function($) {
	Drupal.behaviors.views_table_sorting = {
		attach: function(context, settings) {
			$('.views-table th a').unbind('click').click(function(event) {
				event.preventDefault();
				var link = $(this);

				// Selected ordering
				views_table_sorting_order = (views_table_sorting_order == 'ASC')? 'DESC' : 'ASC';

				// Finding the selects
				var viewWrapper = link.closest('div[class*="view-dom"]');
				var sortBySelect = viewWrapper.find('select[name="sort_by"]');
				var orderSelect = viewWrapper.find('select[name="sort_order"]');

				// Changing select values
				var orderByValue = /&order=[a-zA-Z_]*/.exec(link.attr('href'))[0].split('=')[1];
				sortBySelect.val(orderByValue);
				orderSelect.val(views_table_sorting_order);

				// Submit the form
				orderSelect.closest('form').find('input[type="submit"]').click();
				return false;
			});
		}
	}
})(jQuery);
